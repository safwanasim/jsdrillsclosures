 // Should return a function that invokes `cb`.
 // The returned function should only allow `cb` to be invoked `n` times.
 // Returning null is acceptable if cb can't be returned

 function limitFunctionCallCount(cb, n, numberToBePassed) {
     if (n <= 0) {
         return null
     }

     let result = 0
     for (let index = 0; index < n; index++) {
         result = cb(numberToBePassed) + result
     }
     return result
 }

 module.exports = limitFunctionCallCount