function counterFactory(args) {
    let counter = args
    return {
        increment: function () {
            return ++counter
        },
        decrement: function () {
            return --counter
        }
    }
}

module.exports = counterFactory

//console.log(counterFactory(3).increment())