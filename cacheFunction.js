function cacheFunction(callback) {
    let cache = {}
    return function (...arguments) {
        if (arguments in cache) {
            return cache[arguments]
        } else {
            console.log("Callback Invoked")
            cache[arguments] = callback(arguments)
            return cache[arguments]
        }
    }
}
module.exports = cacheFunction