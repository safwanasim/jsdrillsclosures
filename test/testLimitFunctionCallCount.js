const limitFunctionCallCount = require("../limitFunctionCallCount");

function cb(num) {
    return num * num
}
let numberToBePassed = 5
let invokedTimes = 3
const expectedOutput = (numberToBePassed * numberToBePassed) * invokedTimes


const actualOutput = limitFunctionCallCount(cb, invokedTimes, numberToBePassed)

if (expectedOutput == actualOutput) {
    console.log("Test Passed")
} else {
    console.log("Test Failed")
}


//------------------Testing for null -------------------
invokedTimes = 0
if (limitFunctionCallCount(cb, invokedTimes, numberToBePassed) == null) {
    console.log("Test Passed for null")
} else {
    console.log("Test failed for null")
}