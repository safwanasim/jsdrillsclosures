const cacheFunction = require("../cacheFunction");


function sum(arr) {
    return arr.reduce((a, b) => a + b, 0);

}

var sumMemoize = cacheFunction(sum)
console.log(sumMemoize(100, 30, 40, 50))
console.log(sumMemoize(100, 30, 40, 50))