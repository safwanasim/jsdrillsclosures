const counterFactory = require("../counterFactory");

//---------------Increment Testing---------------------
let expectedOutput = 5
let resultObtained = counterFactory(4).increment()

if (expectedOutput == resultObtained) {
    console.log("Increment Passed")
} else {
    console.log("Increment Failed")
}


//---------------Decrement Testing---------------------
expectedOutput = 3
resultObtained = counterFactory(4).decrement()

if (expectedOutput == resultObtained) {
    console.log("Decrement Passed")
} else {
    console.log("Decrement Failed")
}